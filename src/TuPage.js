
var TuPage = cc.LayerColor.extend({
	
	 init: function() {
		this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
 		console.log( 'Main Page created!s' );
 		var bg = new cc.Sprite(res.main);
        bg.setPosition( new cc.Point(400,300))
        this.addChild(bg);
        var tu = new cc.Sprite(res.tutorial);
        tu.setPosition( new cc.Point(400,300))
        this.addChild(tu);

        this.scoreLabel = new cc.LabelTTF("Click !!!", res.fireball, 50);
        this.scoreLabel.setPosition(cc.p(400,100));
        this.addChild(this.scoreLabel);
        this.addMouseHandlers();
        return true;
	},

	addMouseHandlers:  function(){
        var self = this;
        
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
            
            onMouseMove: function(event){

            },

            onMouseUp: function(event){

            },
            onMouseDown: function(event){
            		 cc.director.runScene(new GameScene());
            },
            onMouseScroll: function(event){

            }

        },this)
    },


})
  var TuScene = cc.Scene.extend({
        onEnter: function() {
            this._super();
            var layer = new TuPage();
            layer.init();
            this.addChild( layer );

        }
});