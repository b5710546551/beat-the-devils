var res = {
	bg : 'res/spiral-galaxy2.jpg',
	fireball: 'res/fireball.png',
	blueball: 'res/blueball.png',
	blackball: 'res/bball.png',
	blackblueball: 'res/bbball.png',
	galaxyball: 'res/galaxyball.png',
	bullet: 'res/bullet10.png',
	ufo: 'res/ufo60.png',
	effect1: 'res/effect1.png',
	effect2: 'res/effect2.png',
	main: 'res/mainbg.jpg',
	start1: 'res/start.png',
	white: 'res/white.png',
	black: 'res/black.png',
	tutorial: 'res/tutorial.png'
 };
 
var g_resources = [];

for (var i in res) {
    g_resources.push(res[i]);
}