
var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
 		console.log( 'GameLayer created!s' );
        this.ballArray = [];
        this.score = 0;
        this.live = 5;
        this.angle=0;
        var size = cc.winSize;
        var bg = new cc.Sprite(res.bg);
        bg.setPosition( new cc.Point(400,300))
        this.addChild(bg);

        this.scoreLabel = new cc.LabelTTF("Score:" + " "+this.score, res.fireball, 40);
        this.scoreLabel.setPosition(cc.p(90,555));
        this.addChild(this.scoreLabel);

        this.liveLabel = new cc.LabelTTF("lives:" + " "+this.live, res.fireball, 35);
        this.liveLabel.setPosition(cc.p(90,500));
        this.addChild(this.liveLabel);

        this.cursorMouse = new cc.Sprite.create(res.whiteBall2)
        this.cursorMouse.setPosition( cc.p(size.width/2,size.height/2) );        
        this.cursorMouse.setAnchorPoint(cc.p(0.5,0.5));
        this.addChild(this.cursorMouse,0)

        this.ufo = new cc.Sprite.create(res.ufo)
        this.ufo.setPosition( cc.p(size.width/2,size.height/2) );        
        this.ufo.setAnchorPoint(cc.p(0.5,0.5));
        this.addChild(this.ufo,0)

        this.cursorMouse_actionUp =  cc.RotateBy.create(0.2,10);
        this.cursorMouse_actionDown =  cc.RotateBy.create(0.2,-10);

        this.speed =70;
        this.addKeyboardHandlers();
        this.scheduleUpdate();
        this.addMouseHandlers();

        this.ballArray = [];
        this.bulletArray = [];
        this.effectArray=[];
        return true;
    },

    update: function(dt){

        if(this.live==0)
            cc.director.runScene(new EndScene());
        
        if(this.score>30)
            this.speed =60;
        else if(this.score>75)
            this.speed =50;
        else this.speed = 40

        for (var i =0 ; i < this.bulletArray.length;i++) {
            var indexBullet =-99;

            var posX = this.bulletArray[i].getPosition().x
            var posY = this.bulletArray[i].getPosition().y
            
            for (var j =0 ; j < this.ballArray.length;j++) {
                    this.ballArray[j].setAnchorPoint(cc.p(0.5,0.5));
                    var point1 = this.ufo.getPosition();
                    var point2 = this.ballArray[j].getPosition()
                    var point3 = this.bulletArray[i].getPosition()
                    var isCrashEnemyAndUFO = cc.pDistance(point1,point2) < 45
                    var isCrashEnemyAndBullet = cc.pDistance(point2,point3) < 20

                    if( isCrashEnemyAndUFO ) {
                        // var eff1 = new EffectUFO(point1)
                        this.removeChild(this.ballArray[j])
                        this.ballArray.splice(j,1)
                        j--;
                        this.live--;
                        this.liveLabel.setString("lives:" + " "+this.live)
                        // this.addChild(eff1)
                        // this.effectArray.push(eff1)
                    }

                    if( isCrashEnemyAndBullet ) {
                        // var eff2 = new EffectEnemy(point2)
                        this.removeChild(this.ballArray[j])
                        this.ballArray.splice(j,1)
                        indexBullet = i;
                        j--;
                        // this.addChild(eff2)
                        // this.effectArray.push(eff2)
                    }
                    // for(var l=0;l<this.effectArray.length;l++){
                    //     if(this.effectArray[l].getCount()==10)
                    //         this.removeChild(this.effectArray[l])
                    // }
            }
            

            if(indexBullet!=-99){
                this.bulletArray[i].removeFromParent();
                this.bulletArray.splice(i,1)
                i--;
                this.score++;
                var msg = "Score:" + " "+ this.score
                this.scoreLabel.setString(msg)
            }

            // if(posX>800||posY>600||posX<0||posY<0){
            //         this.bulletArray[i].removeFromParent();
            //         this.bulletArray.splice(i,1)
            //         i--;
            // }
        }   

        if(count%this.speed==0){
            var bball1 = new Enemy();
            var n = Math.random()
            if(n>0.75)
            bball1.setPosition(new cc.Point( Math.random()*10+Math.random()*100+Math.random()*1000,0))
            else if(n>0.50)
            bball1.setPosition(new cc.Point( Math.random()*10+Math.random()*100+Math.random()*1000,600))
            else if(n>0.25)
            bball1.setPosition(new cc.Point( 0,Math.random()*10+Math.random()*100+Math.random()*1000))
            else
            bball1.setPosition(new cc.Point( 800,Math.random()*10+Math.random()*100+Math.random()*1000))

            this.addChild(bball1)
            bball1.move();
            this.ballArray.push(bball1);

        }

        count++;
        totalScore = this.score
    },

    addMouseHandlers:  function(){
        var self = this;
        
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
            
            onMouseMove: function(event){
                var angle = -Math.atan2(event.getLocationY()-300,event.getLocationX()-400)*(180/Math.PI)
                self.ufo.setRotation(angle);

            },

            onMouseUp: function(event){

            },
            onMouseDown: function(event){
                self.angle = Math.atan2(event.getLocationY()-300,event.getLocationX()-400)
                var fire = new Bullet(self.angle);    
                self.addChild(fire)
                self.bulletArray.push(fire);
            },
            onMouseScroll: function(event){

            }

        },this)
    },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
            }
        }, this);
    }
    ,
      onKeyDown: function( keyCode, event ) {

        if(keyCode == 49){

            var fireKey = new Bullet(3.16);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(1.55);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(-1.55);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(0);
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(-0.75);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(0.75);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(2.3);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(-2.3);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(-0.3875);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(0.3875);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(2.6875);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
            var fireKey = new Bullet(-2.6875);    
            this.addChild(fireKey)
            this.bulletArray.push(fireKey);
        
        }
        if(keyCode == 50){
        }
        if(keyCode == 51){
        }
    },


});
    var totalScore =0;
    var count=0;
    var GameScene = cc.Scene.extend({
        onEnter: function() {
            this._super();
            var layer = new GameLayer();
            layer.init();
            this.addChild( layer );

        }
});