
var Bullet = cc.Sprite.extend({
	
	ctor: function(b){
		this._super();
        this.initWithFile( res.bullet );
        this.scheduleUpdate();
        this.bulletAngle = b;
        this.setPosition(cc.p(400,300))
	},

	update: function(dt){
		
        var position = this.getPosition();
        var a = 20;
		var newPosition = cc.p( a*Math.cos(this.bulletAngle)+position.x , a*Math.sin(this.bulletAngle)+position.y)
		this.setPosition(newPosition);

	}

})
