
var EndPage = cc.LayerColor.extend({
	
	 init: function() {
		this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
 		console.log( 'Main Page created!s' );
 		var bg = new cc.Sprite(res.main);
        bg.setPosition( new cc.Point(400,300))
        this.addChild(bg);

        var white = new cc.Sprite(res.white);
        white.setPosition( new cc.Point(400,300))
        this.addChild(white);

        this.scoreLabel = new cc.LabelTTF("Score", res.fireball, 40);
        this.scoreLabel.setPosition(cc.p(400,320));
        this.addChild(this.scoreLabel);

        this.scoreLabel2 = new cc.LabelTTF(totalScore, res.fireball, 40);
        this.scoreLabel2.setPosition(cc.p(400,280));
        this.addChild(this.scoreLabel2);


        this.scoreLabel3 = new cc.LabelTTF("Click to play again", res.fireball, 40);
        this.scoreLabel3.setPosition(cc.p(400,100));
        this.addChild(this.scoreLabel3);

        this.addMouseHandlers();
        return true;
	},

	addMouseHandlers:  function(){
        var self = this;
        
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
            
            onMouseMove: function(event){

            },

            onMouseUp: function(event){

            },
            onMouseDown: function(event){
            
            		 cc.director.runScene(new GameScene());
            },
            onMouseScroll: function(event){

            }

        },this)
    },


})
  var EndScene = cc.Scene.extend({
        onEnter: function() {
            this._super();
            var layer = new EndPage();
            layer.init();
            this.addChild( layer );

        }
});