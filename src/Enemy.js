var Enemy = cc.Sprite.extend({

    ctor: function() {
        this._super();
        var n = Math.random()
        if(n>0.75)
            this.initWithFile( res.galaxyball );
        else if(n>0.50)
            this.initWithFile( res.blueball );
        else if(n>0.25)
            this.initWithFile( res.blackblueball );
        else
            this.initWithFile( res.fireball );

    },

    move: function(){
        this.animFrames = [];
        this.str = "";
        for (var i = 0; i < 5; i++) {
            this.str = "player-walk-f-" + i;
            this.spriteFrame = cc.spriteFrameCache.getSpriteFrame(this.str);
            this.animFrame = new cc.AnimationFrame();
            this.animFrame.initWithSpriteFrame(this.spriteFrame, 1, null);
            this.animFrames.push(this.spriteFrame);
        }

        this.animation = cc.Animation.create(this.animFrames, 0.025);
        this.animate   = cc.animate(this.animation);

        this.sprite_action = cc.MoveTo.create(3,cc.p(400,300));
        this.runAction(this.sprite_action);
        this.runAction(this.animate);
    },

    update: function( dt ) {
        
    }

  
});

